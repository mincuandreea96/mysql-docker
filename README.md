# mysql-docker 
# 1. how to run docker-mysql 
docker-compose up 

#2 run mysql in detach mode in order to persit the data from the volume
docker-compose up -d

#is keeping the persistance of the data from mysql
docker-compose stop 

#is not persisting the data from the volume after restarting the mysql docker containter
docker-compose down


